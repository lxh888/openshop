#### 返回语言字符串
~~~
//无参数的
$language1 = language('db_key_exists');
//带参数的
$language2 = language('db_class_exists', 'class', 'namespace');
printexit("info1:".$language1, "info2:". $language2);
~~~

~~~
/* ******************** 打印结果 ******************** */
info1:标识不存在！需要传入一个合法的标识！
info2:class 类不合法！( namespace 类未定义) 检查配置信息的数据库类型是否正确！
~~~