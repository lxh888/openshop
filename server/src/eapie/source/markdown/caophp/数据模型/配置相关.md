#### 默认数据库配置


```
Array(
    //数据库类型
    [type] => mysql
    //主机
    [host] => 127.0.0.1
    //数据库用户名
    [user] => 
    //数据库密码
    [pass] => 
    //数据库名称
    [base] => 
    //端口
    [port] => 3306
    //设置数据库的字符集
    [charset] => utf8
    //表前缀
    [prefix] => 
    //是否打开持久链接
    [persistent] => 
    //初始连接时，数据库不存在则自动创建
   	[base_no_exists_create] => 
    //数据库引擎(该参数对pdo有作用)
    [engine] => mysql
    //数据库缓存文件保存在运行缓存目录的文件夹名称。
    [cache_folder_name] => db
    //是否记录method执行日志信息。true记录(默认)，false不记录。
    [method_log] => 1
    //是否记录query语句执行日志信息。true记录(默认)，false不记录。
    [query_log] => 1
    //单位秒。设置锁的有效时间（时间越长，如果存在死锁的时候很危险）
    [lock_time] => 60
    //是否锁文件超时便显示错误提示并终止程序。默认false不显示不终止只记录
    [lock_alert] =>
    //是否锁文件超时便删除。默认true删除。(注意，任何进程都有删除权限。)
    [lock_unlink] => 1
    //是否在 操作事务 回滚的时候 写入日志文件。true记录(默认)，false不记录。
    [work_rollback_log_file] => 1
    
    /*数据库会话模块*/
    [session] => Array (
        //定义初始化的session的id，是一个闭包函数。必须返回字符串
        [id] => 
        //定义失效的时间，是一个闭包函数。必须返回整数
        [expire_time] => Closure Object ()
        //是否开启自动清理，默认true开启，false不开启
        [clear] => 1
        //为true时，如果数据表不存在则自动创建。false不创建
        [found] => 1
        //自动创建数据表时，是否生成日志文件(是一个SQL文件)
        [found_log_file] => 1
        //表结构
        [table] => Array(
        	
            /* 继承上面设置的表前缀和表字符编码 */
            
            //表名称
            [name] => session
            //表备注
            [comment] => 会话信息表
            //表引擎
            [engine] => MyISAM
            //表字段列表
            [field] => Array(
                /*接受下面类型字段：*/
				//id：[必须]创建类型是varchar(155) 主键和唯一键索引
				//expire_time：[必须]失效的时间字段。创建类型是bigint(20) 普通索引
				//found_time：[必须]创建时间字段。创建类型是bigint(20) 普通索引
				//now_time：[必须]当前时间字段。创建类型是bigint(20) 普通索引
				//var：可变长字符串数据。创建类型是varchar(255) 普通索引
				//json：json数据。创建类型是text
				//serialize：序列化数据。创建类型是text
                //state：状态。创建类型是tinyint(1)
                [session_id] => id
                [session_ip] => var
                [user_id] => var
                [session_agent] => var
                [session_json] => json
                [session_serialize] => serialize
                [session_expire_time] => expire_time
                [session_found_time] => found_time
                [session_now_time] => now_time
            )

        )

    )

)
```
