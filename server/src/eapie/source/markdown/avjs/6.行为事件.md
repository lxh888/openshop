[TOC]
行为事件分为公共事件与工程事件。同时存在的时候，先执行公共事件，再执行工程事件。
## 公共事件
| 事件函数/参数   |  事件名称/功能  |  描述 |
| --- | --- | --- |
|  error(message,project)   |  当发生错误时触发 | message是错误信息，project是出错的工程资源，有可能为空 |
|  includeStart(resource)   | 当引入文件开始时触发| resource是av.include()对象资源 |
|  includeEnd(resource,bool)   | 当引入文件完成时触发| resource是av.include()对象资源。引入成功时bool为true，失败时bool为false |
|  exportStart(url)   | 当导入文件开始时触发| url是需要导入的文件地址。注意一点：如果av.framework().export[url] 设为非undefined值，那么则不会执行导入完成时的事件了 |
|  exportEnd(url,bool)   | 当导入文件完成时触发| url是需要导入的文件地址。导入成功时bool为true，失败时bool为false |
|  loadStart(project)   | 页面开始加载时触发| project是这个加载中的工程资源，av().run()会触发 |
|  loadEnd(project)   | 页面加载完成时触发| project是这个加载中的工程资源，av().run()会触发 |
|  resize(windowWidth, windowHeight)   | 窗口或框架被重新调整大小时触发| windowWidth窗口的宽度，windowHeight窗口的高度 |
|  ready(project)   | 当工程准备好的时候（初始化完成的时候）触发| project是当前加载的工程资源，也就是该工程第一次被引入的时候执行 |
|  show(project)   | 用户进入该页面的时触发| project是当前加载的工程资源 |
|  hide(project)   | 用户离开该页面的时触发| project是当前加载的工程资源 |
|  page(href,callback)   |  当页面路由初始化、更新时触发 | 重写av.framework().page参数，自定义路由加载。href是经过处理的路由参数，已经将av.router().anchor.query清除掉了。callback是回调函数，注意！必须要将渲染的工程信息传入该回调函数。数据结构：{id:"工程ID",file:"工程文件"}，如：callback({id:"工程ID",file:"工程文件"}) |
|  pageStart()   |  当页面开始执行时触发 | av().run()开始执行时 |
|  pageEnd()   |  当页面完成执行时触发 | av().run()完成执行时，如果被停止运行了，该事件是不会执行的 |
|  pageNotFound(href)   |  当页面不存在时触发 | href是经过处理的路由参数，已经将av.router().anchor.query清除掉了  |
|  pageChange(router,callback)   | 当页面更新时触发| router是av.router()对象资源。callback是回调函数，在函数定义中逻辑走完了一定要执行callback这个回调函数，否则视为阻止页面更新 |
|  routerChange(router)   | 当路由锚点query参数更新时触发| router是av.router()对象资源。路由中path值变化时，只触发pageChange事件，而只是query参数变化才会触发routerChange事件  |
|  renderStart(project)   | 工程渲染开始时触发| project是当前加载的工程资源 ，av().render()会触发|
|  renderEnd(project)   | 工程渲染结束时触发| project是当前加载的工程资源，av().render()会触发 |

## 工程事件
| 事件函数/参数   |  事件名称/功能  |  描述 |
| --- | --- | --- |
|  error(message)   |  当发生错误时触发 | message是错误信息 |
|  loadStart()   | 当工程开始加载时触发| av().run()会触发 |
|  loadEnd()   | 当工程加载完成时触发| av().run()会触发 |
|  resize(windowWidth, windowHeight)   | 窗口或框架被重新调整大小时触发| windowWidth窗口的宽度，windowHeight窗口的高度 |
|  ready()   | 当工程准备好的时候（初始化完成的时候）触发| 该工程第一次被引入的时候执行 |
|  show()   | 监听页面显示 | 从隐藏到显示便触发 |
|  hide(project)   | 监听页面隐藏 | 从显示到隐藏便触发 |
|  pageChange(router,callback)   | 当页面更新时触发| router是av.router()对象资源。callback是回调函数，在函数定义中逻辑走完了一定要执行callback这个回调函数，否则视为阻止页面更新 |
|  routerChange(router)   | 当路由锚点query参数更新时触发| router是av.router()对象资源。路由中path值变化时，只触发pageChange事件，而只是query参数变化才会触发routerChange事件  |
|  renderStart()   | 工程渲染开始时触发| project是当前加载的工程资源 ，av().render()会触发|
|  renderEnd()   | 工程渲染结束时触发| project是当前加载的工程资源，av().render()会触发 |


## page事件重写av.framework().page参数
```
page:function(href, callback){
    //一定要执行回调函数，否则不回往下执行
    return callback({id:'book', file:'markdown/book.js'});
},
```


